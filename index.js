/*
Modules and parameterized routes

Separation of Concerns:
 -routes
- model
-controllers

Models
-contains WHAT objects are needed in our API (ex:users,courses)
-Objects schemas and relationships are defined here.

Routes
- defines WHEN particular controllers will be used
- A specific controller action will be called when a specific http method is received on a specific API endpoint.

Controllers
-Contains intructions on HOW your API will perform its intended tasks
-Mongoose model queries are used here, examples are:
	-Model.find()
	-Model.findByIdAndDelete()
	-Model.findByIdandUpdate()
*/

// Setup the dependencies
const express= require('express');
const mongoose= require('mongoose');

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute');

// Server setup 
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://justinedeguzman279:admin123@zuitt-bootcamp.qulynt4.mongodb.net/s36?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open",()=> console.log(`Now connected to the cloud database`));

// Add task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, ()=>console.log(`Now listening to port ${port}.`));
