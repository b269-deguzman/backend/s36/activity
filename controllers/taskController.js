// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

const Task = require('../models/task');

// Controller function for getting all the tasks
module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=> {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name,
	})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return task
		}
	})
}

// Controller function for deleting a task
/*
Business Logic
1.Look for the task with the corresponding id provided in the URL/route
2.Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask =(taskId)=>{
	return Task.findByIdAndRemove(taskId).then((removedTask, error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return "Deleted Task."
		}
	})
};

// [SECTION ACTIVITY]
// Controller function for retrieving a specific task
module.exports.getSpecificTask= (taskId)=>{
	return Task.findById(taskId).then((specificTask,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return specificTask;
		}
	})
}

// Controller function for changing the status of a task to complete
module.exports.updateTask= (taskId)=>{
	return Task.findByIdAndUpdate(taskId,{$set: {status: "completed"}}).then((updatedTask,error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			return updatedTask;
		}
	})
}
